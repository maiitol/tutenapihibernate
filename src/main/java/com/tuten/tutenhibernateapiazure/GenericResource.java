/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuten.tutenhibernateapiazure;


import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import com.tuten.tutenhibernateapiazure.model.Email;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;



import com.tuten.tutenhibernateapiazure.session.Task;
import com.tuten.tutenhibernateapiazure.session.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * REST Web Service
 *
 * @author maiitol
 */
@Path("rest")
public class GenericResource {
    
    /*String connectionUrl = "jdbc:sqlserver://tutenbd.database.windows.net:1433;databaseName=BDTuten"; // update me
    String userName = "tuten_user"; // update me
    String password = "KXP9su#Tb-"; // update me*/
    String connectionUrl = "";
    String userName = ""; 
    String password = "";

    @Context
    private UriInfo context;
    
    private final static Logger LOGGER = Logger.getLogger(GenericResource.class.getName());
    
    

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
        this.connectionUrl = System.getenv("SQLCONNSTR_connectionUrl");
        this.userName = System.getenv("SQLCONNSTR_userName");
        this.password = System.getenv("SQLCONNSTR_password");
        
    }

    /**
     * Retrieves representation of an instance of com.tuten.tutenhibernateapiazure.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    @POST
    @Path("/email")
    @Consumes("application/json")
    @Produces("application/json")
    public Response email(Email email)
    {
       
        /*try
        {
            return Response
            .status(Response.Status.OK)
            .entity(System.getenv("prueba") + "-" +
                    System.getenv("SQLCONNSTR_BDTutenPool"))
            .build();
            // SQLCONNSTR_BDTutenPool BDTutenPool
        }catch(Exception err)
        {
            return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(err.getMessage())
                .build(); 
        }*/
        
        
        
        MandrillApi mandrillApi = new MandrillApi("gJQtrzmPlC3Klf9vbBxFGg");

        // create your message
        MandrillMessage message = new MandrillMessage();
        message.setSubject(email.getSubject());
        message.setHtml(email.getMsg());
        message.setAutoText(true);
        message.setFromEmail(email.getFromEmail());
        message.setFromName(email.getFromName());
        
        ArrayList<MandrillMessage.Recipient> recipients = new ArrayList<MandrillMessage.Recipient>();
        MandrillMessage.Recipient recipient = null; 
   
        for(int i = 0; i < email.getEmailsSend().size(); i++)
        {
            recipient = new MandrillMessage.Recipient();
            recipient.setEmail(email.getEmailsSend().get(i).toString());
            recipients.add(recipient);
        }
        
        message.setTo(recipients);
        message.setPreserveRecipients(true);
        
        //recipient.setName("Michael Valencia Outlook");
      
        ArrayList<String> tags = new ArrayList<String>();
        tags.add("tuten");
        tags.add("tuten.cl");
        message.setTags(tags);
        
        
        try
        {
            MandrillMessageStatus[] messageStatusReports = mandrillApi
		.messages().send(message, false);
        }catch(MandrillApiError err)
        {
            return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(err.getMessage())
                .build(); 
        }catch(IOException err)
        {
            return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(err.getMessage())
                .build();   
        }
        

        sendEmail();
        
        return Response
            .status(Response.Status.OK)
            .entity("Se ha enviado correctamente el email")
            .build();
        
    }
    
    public void sendEmail()
    {
            try{
               // SessionFactory sessionFactory =new Configuration().configure().buildSessionFactory();
               // session =sessionFactory.openSession();
                Configuration cfg = createHibernateConfiguration();
                cfg.addAnnotatedClass(User.class);
                cfg.addAnnotatedClass(Task.class);
                SessionFactory sessionFactory = cfg.buildSessionFactory();
                Session session = sessionFactory.openSession();
                session.beginTransaction();
                User newUser = new User("Juan P", "Prueba");
                session.save(newUser);
                session.getTransaction().commit();

        } catch (Exception e) {
            System.out.println();
            e.printStackTrace();
        }
    }
    

    private Configuration createHibernateConfiguration() {
        Configuration cfg = new Configuration()
                .setProperty("hibernate.connection.driver_class", "com.microsoft.sqlserver.jdbc.SQLServerDriver")
                .setProperty("hibernate.connection.url", this.connectionUrl)
                .setProperty("hibernate.connection.username", this.userName)
                .setProperty("hibernate.connection.password", this.password)
                .setProperty("hibernate.connection.autocommit", "true")
                .setProperty("hibernate.show_sql", "false");

        cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
        cfg.setProperty("hibernate.show_sql", "false");
        cfg.setProperty("hibernate.hbm2ddl.auto", "update");
        return cfg;
    }
}
