/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tuten.tutenhibernateapiazure.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author maiitol
 */
@XmlRootElement
public class Email  implements Serializable {

    private String subject;
    private String msg;
    private String fromEmail;
    private String fromName;
    private List<String> emailsSend;

    public Email() { }

    public Email(String subject, String msg, String fromEmail, String fromName, List<String> emailsSend) {
        this.subject = subject;
        this.msg = msg;
        this.fromEmail = fromEmail;
        this.fromName = fromName;
        this.emailsSend = emailsSend;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public List<String> getEmailsSend() {
        return emailsSend;
    }

    public void setEmailsSend(List<String> emailsSend) {
        this.emailsSend = emailsSend;
    }
    
   
}
